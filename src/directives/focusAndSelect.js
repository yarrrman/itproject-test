export const focusAndSelect = {
    inserted(el) {
        el.focus()
        el.select()
    },
}