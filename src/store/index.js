import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

import { actions } from "./modificationActions";

Vue.use(Vuex)



const createCard = (id = 0) => {
  return {
    id,
    todos: [],
    title:'New note',
    _changeStack: [],
    _stackCursorId: 0,
    _stackLastCursorId: 0,
    _stackUndos: 0,
  }
}



const createCardView = (id = 0) => {
  return {
    id,
    todos: [],
    title:'New note',
    changes: 0,
    undos: 0,
  }
}

const applyModification = (card, modification) => {
    let todo = card.todos.find(t => t.todoId === modification.todoId);
    switch (modification.act) {
      case actions.CHANGE_TITLE:
        card.title = modification.value;
        break;
      case actions.NEW_TODO:
        if (!todo) {
          card.todos.push({
            todoId: modification.todoId,
            done: false,
            text: modification.value,
          });
        }
        break;
      case actions.CHANGE_DONE:
        todo.done = modification.value;  
        break;
      case actions.CHANGE_TODO_TEXT:
        todo.text = modification.value;
        break;
      case actions.DELETE_TODO:
        let todoIndex = card.todos.findIndex(t => t.id === modification.todoId);
        if (todoIndex > -1) {
          card.todos.splice(todoIndex, 1);
        }
        break;

    }
    return card;
}

const applyModifications = (cardView, card, stack) => {
    let _stack = stack.slice(0, card._stackCursorId - card._stackUndos);
    console.log('stack', _stack);
    _stack.forEach(mod => {
      cardView = applyModification(cardView, mod);
    })

    cardView.changes = card._stackCursorId - card._stackUndos;
    cardView.undos = card._stackUndos;
    return cardView;
}

export default new Vuex.Store({
  state: {
    maxId: 0,
    cards: [],
    currentCard: null,
    newCard: {},
  },
  plugins: [createPersistedState()],
  mutations: {
    cancelEditCard (state, {id}) {
      let card;
      if (id == 0) {
        state.newCard = {}
      } else {
        card = state.cards.find(card => card.id == id);
        card._changeStack.splice(card._stackLastCursorId);
        card._stackCursorId = card._stackLastCursorId
      }
    },

    undoTodo (state, {id}) {
      let card;
      if (id == 0) {
        card = state.newCard;
      } else {
        card = state.cards.find(card => card.id == id);
      }
      
      if (card._stackUndos < card._stackCursorId ) {
        card._stackUndos += 1;
      }
    },  
    redoTodo (state, {id}) {
      let card;
      if (id == 0) {
        card = state.newCard;
      } else {
        card = state.cards.find(card => card.id == id);
      }
      
      if (card._stackUndos > 0 ) {
        card._stackUndos -= 1;
      }
    },
    
    
    changeTodo (state, {id, modification}) {

      let card;
      
      if (id == 0) {
        card = state.newCard;
      } else {
        card = state.cards.find(card => card.id == id);
      }

      card._changeStack = card._changeStack.slice(0, card._stackCursorId);
      card._changeStack.push(modification);
      card._stackCursorId = card._changeStack.length;
    },

    clearNewCard(state) {
      state.newCard = createCard(0);
    },
    newCard (state, payload) {
      state.newCard = createCard();
      console.log('mutation create new card', state.newCard);
    },
    saveCard (state, {id}) {
      console.log('mutation save card');
      let card;
      if (id == 0) {
        card = state.newCard;
        card.id = ++state.maxId;
        card._stackLastCursorId = card._stackCursorId;
        state.cards.push(card);
        state.newCard = {};
      } else {
        card = state.cards.find((card) => card.id == id);
        card._stackLastCursorId = card._stackCursorId;
      }
    },
    deleteCard (state, payload) {
      let idx = state.cards.findIndex(card => card.id == payload.id);
      if (idx > -1) {
        state.cards.splice(idx,1);
      }
    },

    incrementMaxId (state) {
      state.maxId += 1;
    },

  },
  actions: {
    saveCard ({commit}, payload) {
      console.log('action save card');
      commit('saveCard', payload);
    },
    cancelEditCard ({commit}, payload) {
      console.log('action cancel edit card');
      commit('cancelEditCard', payload);
    },
    deleteCard ({commit}, payload) {
      console.log('action delete card');
      commit('deleteCard', payload);
    },
    newCard ({state, commit}, payload) {
      console.log('action new card');
      commit('newCard');
    },

    applyModificationsToCard () {},
    
    changeTitle ({commit}, payload) {
      console.log('action change title');
      payload.modification.act = actions.CHANGE_TITLE;
      commit('changeTodo', payload);
    },
    checkTodo ({commit}, payload) {
      console.log('action check todo');
      payload.modification.act = actions.CHANGE_DONE;
      commit('changeTodo', payload);
    },
    addTodo ({commit, state}, payload) {
      console.log('action add todo');
      commit('incrementMaxId');
      payload.modification.act = actions.NEW_TODO;
      payload.modification.todoId = state.maxId;
      commit('changeTodo', payload);
    },
    changeTextTodo ({commit}, payload) {
      console.log('action change text todo');
      payload.modification.act = actions.CHANGE_TODO_TEXT;
      commit('changeTodo', payload);
    },
    deleteTodo ({commit}, payload) {
      console.log('action delete todo');
      payload.modification.act = actions.DELETE_TODO;
      commit('changeTodo', payload);
    },
    undoTodo ({commit}, payload) {
      console.log('action undo')
      commit('undoTodo' , payload);
    },
    redoTodo ({commit}, payload) {
      console.log('action redo');
      commit('redoTodo' , payload);
    },
  },
  getters: {
    getCards (state) {
       if( !state.cards || !state.cards.length ) return [];
       let cardsView = state.cards.map(card => {
        let stack = card._changeStack;
        let cardView = applyModifications(createCardView(card.id), card,  stack);
        cardView.todos = cardView.todos.slice(0, 5);
        return cardView;
      });
      console.log('getter cards view', cardsView);
      return cardsView   
    },
    getCard: state => id => {
      let card = {};
      if (id === 0) {
        card = state.newCard;
      } else {
        card  = state.cards.find(c => c.id == id );
      }

      if (!card) return {};

      let stack = card._changeStack;
      let cardView = applyModifications(createCardView(card.id), card, stack)
      
      return cardView || {};
    },
    getNextId (state) {
      return state.maxId + 1;
    },
    
  },
  modules: {

  }
})
