export const actions = {
    CHANGE_TITLE: 'changeTitle',
    CHANGE_DONE: 'changeDone',
    CHANGE_TODO_TEXT: 'changeTodoText',
    DELETE_TODO: 'deleteTodo',
    NEW_TODO: 'newTodo',
}